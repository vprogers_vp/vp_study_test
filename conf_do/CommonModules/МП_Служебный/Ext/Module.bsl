﻿
#Область ПрограммныйИнтерфейс

// Проверяет наличие у клиента синхронизируемых областей данных.
// 
// Возвращаемое значение:
//  Булево - Истина если есть что синхронизировать.
//
Функция УКлиентаЕстьСинхронизируемыеОбласти() Экспорт

	Пользователь = ПользователиКлиентСервер.ТекущийПользователь();
	
	СинхронизироватьПочту = 
		РегистрыСведений.ОбменСМобильнымиНастройкиПользователей.ПолучитьНастройку(
			Пользователь,
			Перечисления.ОбменСМобильнымиТипыНастроекПользователей.СинхронизацияПочты);

	Если СинхронизироватьПочту Тогда
		Возврат Истина;
	КонецЕсли;

	СинхронизироватьЗадачи = 
		РегистрыСведений.ОбменСМобильнымиНастройкиПользователей.ПолучитьНастройку(
			Пользователь,
			Перечисления.ОбменСМобильнымиТипыНастроекПользователей.СинхронизацияЗадач);

	Если СинхронизироватьЗадачи Тогда
		Возврат Истина;
	КонецЕсли;

	СинхронизироватьКалендарь = 
		РегистрыСведений.ОбменСМобильнымиНастройкиПользователей.ПолучитьНастройку(
			Пользователь,
			Перечисления.ОбменСМобильнымиТипыНастроекПользователей.СинхронизацияКалендаря);

	Если СинхронизироватьКалендарь Тогда
		Возврат Истина;
	КонецЕсли;

	СинхронизироватьКонтроль = 
		РегистрыСведений.ОбменСМобильнымиНастройкиПользователей.ПолучитьНастройку(
			Пользователь,
			Перечисления.ОбменСМобильнымиТипыНастроекПользователей.СинхронизацияКонтроля);

	Если СинхронизироватьКонтроль Тогда
		Возврат Истина;
	КонецЕсли;

	Возврат Ложь;

КонецФункции

#КонецОбласти // ПрограммныйИнтерфейс

#Область СлужебныеПроцедурыИФункции

// Инициирует обработку входящего сообщения и заполнения ответного сообщения.
//
// Параметры:
//  МобильноеПриложение	 - ссылка на элемент справочника ПользователиМобильныхПриложений;
//  ИсходящееСообщение	 - Ссылка на элемент справочника СообщенияИнтегрированныхСистем.
//
Процедура ВыполнитьСинхронизациюДанных(МобильноеПриложение, СтруктурыВходящихСообщений,
	ФормироватьПакетыОбмена = Истина) Экспорт
	
	ПараметрыСинхронизации = МП_СлужебныйПовтИсп.ПараметрыСинхронизации(МобильноеПриложение);
	
	Попытка
		
		МП_ОбработкаВходящегоСообщения.ЗаписатьИОбработатьВходящиеСообщения(МобильноеПриложение,
			СтруктурыВходящихСообщений, ПараметрыСинхронизации);
			
		Если формироватьПакетыОбмена Тогда
			ЕстьСинхронизируемыеОбласти = МП_Служебный.УКлиентаЕстьСинхронизируемыеОбласти();
			
			Если Не ЕстьСинхронизируемыеОбласти Тогда
				ТекстПредупреждения = СтрШаблон("warning:%1",
					НСтр("ru = 'Не включена синхронизация данных с мобильным клиентом.
					|C сервера на мобильный клиент не передаются никакие данные.
					|Необходимо зайти в персональные настройки в настольном клиенте и включить синхронизацию.'"));
					
				Попытка
					
					ВызватьИсключение ТекстПредупреждения;
					
				Исключение
					
					Инфо = ИнформацияОбОшибке();
					
					МП_ФормированиеИсходящегоСообщения.ПоместитьВОчередьСообщениеОбОшибке(МобильноеПриложение,
						Инфо);
					
				КонецПопытки;
			КонецЕсли;

			МП_ФормированиеИсходящегоСообщения.ЗапуститьФормированиеСообщенийОбмена(МобильноеПриложение,
				ПараметрыСинхронизации);
			
		КонецЕсли;
		
	Исключение

		Инфо = ИнформацияОбОшибке();

		Если Инфо.Описание = "СтопДальнейшейОбработки" Тогда
			Возврат;
		КонецЕсли;

		ЗаписьЖурналаРегистрации(НСтр("ru = 'Обмен с мобильным.Обработка сообщения.Ошибка'", 
			Метаданные.ОсновнойЯзык.КодЯзыка), УровеньЖурналаРегистрации.Ошибка, ,
			Строка(МобильноеПриложение),
			ПодробноеПредставлениеОшибки(Инфо));
			
		МП_ФормированиеИсходящегоСообщения.ПоместитьВОчередьСообщениеОбОшибке(МобильноеПриложение, Инфо);

		УстановитьПривилегированныйРежим(Истина);

		ВызватьИсключение Инфо;

	КонецПопытки;
	
КонецПроцедуры

//Используется для просмотра сообщения из списка справочника "СообщенияИнтегрированныхСистем"
Функция ПреобразоватьЗначениеВJSON(Значение, Отступ = "") Экспорт
	
	Разделитель = "";
   
    ТипЗн = ТипЗнч(Значение);

    Если ТипЗн = Тип("Строка") Тогда
        Стр = """" + Маскировать(Значение) + """";
		
	ИначеЕсли ТипЗн = Тип("Число") Или ТипЗнч(Значение) = Тип("Булево") Или ТипЗн = Тип("ДвоичныеДанные") 
		Или ТипЗн = Тип("ХранилищеЗначения") Тогда
			
		Стр = """" + XMLСтрока(Значение) + """";

    ИначеЕсли ТипЗн = Тип("Дата") Тогда
        Стр = """" +?(ЗначениеЗаполнено(Значение), XMLСтрока(Значение), "")+"""";

    ИначеЕсли ТипЗн = Тип("Структура") Тогда
		
		Стр = "{";
		
		Для Каждого Параметр Из Значение Цикл
			
			Стр = Стр + Разделитель + Символы.ПС + Отступ + """" + Параметр.Ключ + """:" + 
				ПреобразоватьЗначениеВJSON(Параметр.Значение, Отступ + Символы.Таб);
				
			Разделитель = ",";
			
		КонецЦикла;
		
		Стр = Стр + Символы.ПС + Отступ + "}";
		
	ИначеЕсли ТипЗн = Тип("Массив") Тогда
		
		Стр = "[";
		Для Каждого Элемент Из Значение Цикл
			
			Стр = Стр + Разделитель + Символы.ПС + Отступ + ПреобразоватьЗначениеВJSON(Элемент, Отступ);
			Разделитель = ",";
			
		КонецЦикла;
		
		Стр = Стр + Символы.ПС + Отступ + "]";
		
	ИначеЕсли ТипЗн = Тип("ТаблицаЗначений") Тогда
		
		Колонки = Значение.Колонки;
		
		Массив = Новый Массив;
		
		Для Каждого СтрокаТЗ Из Значение Цикл
            Структура=Новый Структура;
            Для Каждого Колонка Из Колонки Цикл
                Структура.Вставить(Колонка.Имя, СтрокаТЗ[Колонка.Имя])
            КонецЦикла;
            Массив.Добавить(Структура);
        КонецЦикла;
		
		Стр = ПреобразоватьЗначениеВJSON(Массив, Отступ)

    ИначеЕсли Значение=Неопределено Тогда
		
		Стр = "null"
		
	ИначеЕсли ОбщегоНазначения.ЭтоСсылка(ТипЗн) Тогда
		
		Если ОбщегоНазначения.ЭтоСправочник(Значение.Метаданные()) Тогда
			Массив = Новый Массив;
			Массив.Добавить(Значение.Метаданные().Имя);
			Массив.Добавить(Строка(Значение.УникальныйИдентификатор()));
			Массив.Добавить(Строка(Значение.Наименование));
			Стр = ПреобразоватьЗначениеВJSON(Массив, Отступ);
			
		ИначеЕсли ОбщегоНазначения.ЭтоДокумент(Значение.Метаданные()) Тогда
			
			Массив = Новый Массив;
			Массив.Добавить(Значение.Метаданные().Имя);
			Массив.Добавить(Строка(Значение.УникальныйИдентификатор()));
			Массив.Добавить(Строка(Значение.Дата));
			Массив.Добавить(Строка(Значение.Номер));
			Стр = ПреобразоватьЗначениеВJSON(Массив, Отступ);
			
		КонецЕсли; 
		
	Иначе
		Стр = """" + Маскировать(Значение) + """";
    КонецЕсли;

    Возврат Стр
КонецФункции

Функция Маскировать(Знач Стр)
  
	Стр=СтрЗаменить(Стр,Символы.ПС,"\n");
    Стр=СтрЗаменить(Стр,Символы.ВК,"\r");
    Стр=СтрЗаменить(Стр,"""","\""");
    Стр=СтрЗаменить(Стр,"'","\'");
    Возврат Стр

КонецФункции

Функция РазделительПакетногоЗапроса() Экспорт
	
	Возврат "
		|;
		|////////////////////////////////////////////////////////////////////////////////
		|";	
		
Конецфункции	

Функция ЭтоМобильноеПриложениеВерсии22(МобильноеПриложение) Экспорт
	
	 Возврат Лев(РегистрыСведений.СведенияОМобильныхКлиентах.ПолучитьВерсию(МобильноеПриложение), 3) = "2.2";
	
КонецФункции

Функция ОпределитьДатуУстареванияДанных(МобильноеПриложение, ПараметрыСинхронизации = Неопределено) Экспорт
	
	СрокУстареванияДанных = 0;
	
	Если ПараметрыСинхронизации <> Неопределено Тогда
		
		//ПервичнаяСинхронизация
		Если Не ЗначениеЗаполнено(ПараметрыСинхронизации.ОтметкаВремениМобильного) Тогда
			
			ДатаПервоначальнойЗагрузки = НачалоДня(ТекущаяДатаСеанса() - ПараметрыСинхронизации.ПериодПервичнойЗагрузки*86400);
			
			РегистрыСведений.СведенияОМобильныхКлиентах.ЗаписатьДатуПервоначальнойЗагрузкиДанных(МобильноеПриложение, ДатаПервоначальнойЗагрузки);
			
			Возврат ДатаПервоначальнойЗагрузки;
			
		Иначе
			
			СрокУстареванияДанных = ПараметрыСинхронизации.СрокУстареванияДанных;
			
		КонецЕсли;
		
	Иначе
		
		ТекущийПользователь = ПользователиКлиентСервер.ТекущийПользователь();
		
		СрокУстареванияДанных = РегистрыСведений.ОбменСМобильнымиНастройкиПользователей.ПолучитьНастройку(ТекущийПользователь,
			Перечисления.ОбменСМобильнымиТипыНастроекПользователей.СрокУстареванияДанных);
			
	КонецЕсли;
	
	ДатаУстареванияДанных = НачалоДня(ТекущаяДатаСеанса() - (СрокУстареванияДанных * 86400));
	
	ДатаПервоначальнойЗагрузки = РегистрыСведений.СведенияОМобильныхКлиентах.ПолучитьДатуПервоначальнойЗагрузкиДанных(
		МобильноеПриложение);
		
	Если ДатаУстареванияДанных < ДатаПервоначальнойЗагрузки Или СрокУстареванияДанных = 0 Тогда
		
		ДатаУстареванияДанных = ДатаПервоначальнойЗагрузки;
		
	КонецЕсли; 
		
	Возврат ДатаУстареванияДанных;
	
КонецФункции

#КонецОбласти // СлужебныеПроцедурыИФункции

#Область РаботаССообщениямиОбмена

// Выполняет запуск фонового задания обработки данных с мобильного клиента.
//
// Параметры:
//  МобильныйКлиент - ПланОбменаСсылка.Мобильный - Ссылка на узел;
//  ИмяМетода       - Строка - Имя метода который будет запущен как фоновое задание.
//
Процедура ЗапуститьФоновоеЗадание(МобильноеПриложение, ПараметрыФоновогоЗадания, ИмяМетода) Экспорт

	// В клиент-серверном варианте и в файловом 8.3 сообщение принимается и готовится в 
	// фоновом задании. Это позволяет избежать таймаутов на стороне мобильного клиента, 
	// т.к. сообщение может долго приниматься и готовиться.
	
	СтруктураОтбора = Новый Структура("Наименование, Состояние, ИмяМетода", МобильноеПриложение.Код, СостояниеФоновогоЗадания.Активно, 
		ИмяМетода);
	
	МассивЗаданий = ФоновыеЗадания.ПолучитьФоновыеЗадания(СтруктураОтбора);

	ЕстьАктивноеЗадание = МассивЗаданий.Количество() > 0;
	
	Если Не ЕстьАктивноеЗадание Тогда
		
		ФоновоеЗадание = ФоновыеЗадания.Выполнить(ИмяМетода, ПараметрыФоновогоЗадания, 
			Новый УникальныйИдентификатор(), МобильноеПриложение.Код); // наименование фонового задания

	КонецЕсли;

КонецПроцедуры

// Создание нового сообщения интегрированных систем и помещение его в очередь. У сообщения 
// устанавливается признак того, что оно находится в стадии подготовки (процент готовности = 0).
//
// Параметры:
//  МобильныйКлиент - ПланОбменаСсылка.Мобильный - Ссылка на узел;
// 
// Возвращаемое значение:
//  СправочникСсылка.СообщенияИнтегрированныхСистем - Ссылка на сообщение.
//
Функция СоздатьИДобавитьСообщениеВОчередь(МобильноеПриложение) Экспорт

	УстановитьПривилегированныйРежим(Истина);

	Сообщение = Справочники.СообщенияИнтегрированныхСистем.СоздатьЭлемент();
	Сообщение.ИдентификаторСообщения = Новый УникальныйИдентификатор;
	Сообщение.Входящее = Ложь;
	Сообщение.ДатаСоздания = ТекущаяДатаСеанса();
    Сообщение.Записать();

	РегистрыСведений.СтепеньГотовностиСообщенийИнтегрированныхСистем.УстановитьПроцентГотовности(Сообщение, 0);
	
	РегистрыСведений.ОчередиСообщенийОбменаСМобильнымиКлиентами.ПоместитьСообщениеВОчередь(МобильноеПриложение, Сообщение);
	
	Возврат Сообщение.Ссылка;
	
КонецФункции

// Записывает в очередь входящее сообщение
//
// Параметры:
//  МобильныйКлиент 	 - ПланОбменаСсылка.Мобильный - Ссылка на узел;
//  СообщениеОтСервера	 - ОбъектXDTO - Полученные сервером данные.
//
Процедура ЗаписатьВходящееСообщение(МобильноеПриложение, СтруктураВходящегоСообщения) Экспорт

	ДанныеСообщения = СтруктураВходящегоСообщения.ExchangeData;
	
	Если ДанныеСообщения = Неопределено Или ТипЗнч(ДанныеСообщения) <> Тип("ДвоичныеДанные") Тогда
		Возврат;
	КонецЕсли;

	Сообщение = Справочники.СообщенияИнтегрированныхСистем.СоздатьЭлемент();
	Сообщение.ИдентификаторСообщения = СтруктураВходящегоСообщения.ID;
	Сообщение.Входящее = Истина;
	Сообщение.ДатаСоздания = ТекущаяДатаСеанса();
	
	Сообщение.ДанныеСообщения = 
			Новый ХранилищеЗначения(ДанныеСообщения, Новый СжатиеДанных(9));
			
	Сообщение.Записать();

	РегистрыСведений.ОчередиСообщенийОбменаСМобильнымиКлиентами.ПоместитьСообщениеВОчередь(МобильноеПриложение, Сообщение);

	РегистрыСведений.СведенияОСообщенияхОбменаСМобильнымиКлиентами.ЗаписатьРазмер(Сообщение.Ссылка,
		ДанныеСообщения.Размер());

КонецПроцедуры

// Помечает сообщение обмена обработанным
//
// Параметры:
//  Сообщение - Справочник.СообщенияИнтегрированныхСистем - Сообщение обмена данными.
//
Процедура ПометитьСообщениеОбработанным(Сообщение) Экспорт

	СообщениеОбъект = Сообщение.ПолучитьОбъект();
	Если Не СообщениеОбъект = Неопределено Тогда
		
		СообщениеОбъект.ДатаОбработки = ТекущаяДатаСеанса();
		СообщениеОбъект.ПометкаУдаления = Истина;
		СообщениеОбъект.Записать();

	КонецЕсли;

КонецПроцедуры 

#КонецОбласти // Работа с сообщениями обмена