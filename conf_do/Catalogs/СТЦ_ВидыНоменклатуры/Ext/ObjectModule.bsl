﻿////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ 

Процедура ПередЗаписью(Отказ)

	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;

	Если ЭтоГруппа Тогда
		Возврат;
	КонецЕсли;

	УстановитьПривилегированныйРежим(Истина);

	// При необходимости создадим или изменим соответствующий набор свойств
	Если Не Отказ Тогда
		ОбновитьРеквизитНаборСвойств();
		ОбновитьРеквизитНаборСвойствХарактеристик();
		ОбновитьРеквизитНаборСвойствСерий();
	КонецЕсли;
	
	// Подсистема Свойства
	УправлениеСвойствами.ПередЗаписьюВидаОбъекта(ЭтотОбъект, "Справочник_Номенклатура");
	
	//Если Не ИспользоватьХарактеристики Тогда
	//	ИспользованиеХарактеристик = Перечисления.ВариантыВеденияДополнительныхДанныхПоНоменклатуре.НеИспользовать;
	//КонецЕсли;
	//
	//Если Не ИспользоватьСерии Тогда
	//	ПолитикиУчетаСерий.Очистить();
	//	ИспользоватьНомерСерии        = Ложь;
	//	ИспользоватьСрокГодностиСерии = Ложь;
	//	ИспользоватьКоличествоСерии   = Ложь;
	//	ТочностьУказанияСрокаГодностиСерии = Перечисления.ТочностиУказанияСрокаГодности.ПустаяСсылка();
	//	НастройкаИспользованияСерий   = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПустаяСсылка();
	//Иначе
	//	ИспользоватьНомерСерии = (НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ЭкземплярТовара
	//								Или НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеру
	//								Или НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеруИСрокуГодности);
	//								
	//	ИспользоватьСрокГодностиСерии = (НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеруИСрокуГодности
	//										Или НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоСрокуГодности);
	//										
	//	ИспользоватьКоличествоСерии = (НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоСрокуГодности
	//								Или НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеру
	//								Или НастройкаИспользованияСерий = Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеруИСрокуГодности);
	//								
	//	Если Не ИспользоватьСрокГодностиСерии Тогда
	//		ТочностьУказанияСрокаГодностиСерии = Перечисления.ТочностиУказанияСрокаГодности.ПустаяСсылка();
	//	КонецЕсли;
	//КонецЕсли;
	//
	//КонтролироватьДублиНоменклатуры = РеквизитыДляКонтроляНоменклатуры.НайтиСтроки(Новый Структура("Уникален", Истина)).Количество() > 0;
	//
КонецПроцедуры 

Процедура ПриКопировании(ОбъектКопирования)
	
	МетаданныеОбъекта = Метаданные();

	
	НаборСвойств              = Справочники.НаборыДополнительныхРеквизитовИСведений.ПустаяСсылка();
	НаборСвойствХарактеристик = Справочники.НаборыДополнительныхРеквизитовИСведений.ПустаяСсылка();
	НаборСвойствСерий         = Справочники.НаборыДополнительныхРеквизитовИСведений.ПустаяСсылка();
	
	ШаблонНаименованияДляПечатиНоменклатуры 	= МетаданныеОбъекта.Реквизиты.ШаблонНаименованияДляПечатиНоменклатуры.ЗначениеЗаполнения;
	ШаблонРабочегоНаименованияНоменклатуры 		= "";
	ЗапретРедактированияРабочегоНаименованияНоменклатуры 	= Ложь;
	ЗапретРедактированияНаименованияНоменклатурыДляПечати 	= Ложь;
	
	ШаблонНаименованияДляПечатиХарактеристики 	= МетаданныеОбъекта.Реквизиты.ШаблонНаименованияДляПечатиХарактеристики.ЗначениеЗаполнения;
	ШаблонРабочегоНаименованияХарактеристики 	= "";
	ЗапретРедактированияРабочегоНаименованияХарактеристики 	= Ложь;
	ЗапретРедактированияНаименованияХарактеристикиДляПечати = Ложь;
	ШаблоныСостояний.Очистить();
	//ШаблоныТехническихПараметров.Очистить();
	
КонецПроцедуры 

Процедура ОбработкаПроверкиЗаполнения(Отказ, ПроверяемыеРеквизиты)
	МассивНепроверяемыхРеквизитов = Новый Массив;
	
	Если Не ЭтоГруппа Тогда
		
		Если Не Справочники.ГруппыДоступаНоменклатуры.ИспользуютсяГруппыДоступа() Тогда
			МассивНепроверяемыхРеквизитов.Добавить("ГруппаДоступа");
		КонецЕсли;
		
		Если ИспользоватьСерии Тогда
			
			Если НастройкаИспользованияСерий <> Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоНомеруИСрокуГодности
				И НастройкаИспользованияСерий <> Перечисления.СТЦ_НастройкиИспользованияСерийНоменклатуры.ПартияТоваровПоСрокуГодности Тогда
				
				МассивНепроверяемыхРеквизитов.Добавить("ТочностьУказанияСрокаГодностиСерии");
				
			КонецЕсли;
			
			КлючевыеРеквизиты = Новый Массив;
			КлючевыеРеквизиты.Добавить("Склад");
			
			//ОбработкаТабличнойЧастиСервер.ПроверитьНаличиеДублейСтрокТЧ(ЭтотОбъект,"ПолитикиУчетаСерий",КлючевыеРеквизиты,Отказ);
		Иначе
			МассивНепроверяемыхРеквизитов.Добавить("ТочностьУказанияСрокаГодностиСерии");
			МассивНепроверяемыхРеквизитов.Добавить("НастройкаИспользованияСерий");
		КонецЕсли;
		
		//Если Не ИспользоватьСрокГодностиСерии Тогда
		//КонецЕсли;
		
		//Если Не ИспользоватьХарактеристики Тогда
		//	МассивНепроверяемыхРеквизитов.Добавить("ИспользованиеХарактеристик");
		//КонецЕсли;
		
		//Если Не ЗначениеНастроекПовтИсп.ИспользуютсяГруппыДоступаНоменклатуры() Тогда
		//	МассивНепроверяемыхРеквизитов.Добавить("ГруппаДоступа");
		//КонецЕсли;
		
		СтруктураШаблонов = Новый Структура;
		//СтруктураШаблонов.Вставить("ШаблонНаименованияДляПечатиНоменклатуры",   ШаблонНаименованияДляПечатиНоменклатуры);
		//СтруктураШаблонов.Вставить("ШаблонНаименованияДляПечатиХарактеристики", ШаблонНаименованияДляПечатиХарактеристики);
		//СтруктураШаблонов.Вставить("ШаблонРабочегоНаименованияНоменклатуры",    ШаблонРабочегоНаименованияНоменклатуры);
		//СтруктураШаблонов.Вставить("ШаблонРабочегоНаименованияХарактеристики",  ШаблонРабочегоНаименованияХарактеристики);
		ШаблонТекстаОшибки = НСтр("ru='В формуле шаблона ""%ИмяШаблона%"" обнаружены ошибки'");
		МетаданныеОбъекта = ЭтотОбъект.Метаданные();
		
		Для Каждого Элемент Из СтруктураШаблонов Цикл
			Шаблон = Элемент.Значение;
			Если ЗначениеЗаполнено(Шаблон) Тогда
				ТекстОшибки = СтрЗаменить(ШаблонТекстаОшибки, "%ИмяШаблона%", МетаданныеОбъекта.Реквизиты[Элемент.Ключ].Синоним);
				//Если Не РаботаСФормуламиКлиентСервер.ПроверитьФормулу(Шаблон, РаботаСФормуламиКлиентСервер.ПолучитьМассивОперандовТекстовойФормулы(Шаблон), Элемент.Ключ, ТекстОшибки, Истина, "Объект") Тогда
				//	Отказ = Истина;
				//КонецЕсли;
			КонецЕсли;
			
		КонецЦикла;
	КонецЕсли;
	ОбщегоНазначения.УдалитьНепроверяемыеРеквизитыИзМассива(ПроверяемыеРеквизиты, МассивНепроверяемыхРеквизитов);
КонецПроцедуры

Процедура ПриЗаписи(Отказ)
	
	Если ОбменДанными.Загрузка Тогда
		Возврат;
	КонецЕсли;
	
	ОбновитьФлагиИспользованияСерийВСкладах();
КонецПроцедуры

Процедура ОбработкаЗаполнения(ДанныеЗаполнения, СтандартнаяОбработка)
	//Если Не ЭтоГруппа Тогда
	//	ГруппаДоступа = ЗначениеНастроекПовтИсп.ПолучитьГруппуДоступаНоменклатурыПоУмолчанию(ГруппаДоступа);
	//КонецЕсли;
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// СЛУЖЕБНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

////////////////////////////////////////////////////////////////////////////////
// Прочее

Функция НаборСвойствНужноИзменить(НаборСвойств)
	
	Результат = ОбщегоНазначения.ПолучитьЗначенияРеквизитов(НаборСвойств, Новый Структура("Наименование,ПометкаУдаления"));
	
	Возврат (Результат.Наименование <> Наименование) ИЛИ (Результат.ПометкаУдаления <> ПометкаУдаления);
	
КонецФункции 

Процедура ОбновитьРеквизитНаборСвойств()
	
	Если Не ЗначениеЗаполнено(НаборСвойств) Тогда
		ОбъектНабора = Справочники.НаборыДополнительныхРеквизитовИСведений.СоздатьЭлемент();
	Иначе
		
		Если Не НаборСвойствНужноИзменить(НаборСвойств) Тогда
			Возврат;
		КонецЕсли;
		
		ОбъектНабора = НаборСвойств.ПолучитьОбъект();
		
	КонецЕсли;
	
	ОбъектНабора.Наименование    = Наименование;
	ОбъектНабора.Родитель        = Справочники.НаборыДополнительныхРеквизитовИСведений.Справочник_Номенклатура;
	ОбъектНабора.ПометкаУдаления = ПометкаУдаления;
	ОбъектНабора.Записать();
	
	НаборСвойств = ОбъектНабора.Ссылка;
	
КонецПроцедуры 

Процедура ОбновитьРеквизитНаборСвойствХарактеристик()
	
	//Если ИспользованиеХарактеристик = Перечисления.ВариантыВеденияДополнительныхДанныхПоНоменклатуре.ОбщиеДляВидаНоменклатуры
	//	ИЛИ ИспользованиеХарактеристик = Перечисления.ВариантыВеденияДополнительныхДанныхПоНоменклатуре.ИндивидуальныеДляНоменклатуры Тогда
	//
	//	Если Не ЗначениеЗаполнено(НаборСвойствХарактеристик) Тогда
	//		ОбъектНабора = Справочники.НаборыДополнительныхРеквизитовИСведений.СоздатьЭлемент();
	//	Иначе
	//		
	//		Если Не НаборСвойствНужноИзменить(НаборСвойствХарактеристик) Тогда
	//			Возврат;
	//		КонецЕсли;
	//		
	//		ОбъектНабора = НаборСвойствХарактеристик.ПолучитьОбъект();
	//		
	//	КонецЕсли;
	//	
	//	ОбъектНабора.Наименование    = Наименование + НСтр("ru = '(Для характеристик)'");
	//	ОбъектНабора.Родитель        = Справочники.НаборыДополнительныхРеквизитовИСведений.Справочник_ХарактеристикиНоменклатуры;
	//	ОбъектНабора.ПометкаУдаления = ПометкаУдаления;
	//	ОбъектНабора.Записать();
	//	
	//	НаборСвойствХарактеристик = ОбъектНабора.Ссылка;
	//
	//Иначе
	//	
	//	Если ЗначениеЗаполнено(НаборСвойствХарактеристик) Тогда
	//		
	//		ОбъектНабора = НаборСвойствХарактеристик.ПолучитьОбъект();
	//		
	//		ОбъектНабора.Наименование    = Наименование + НСтр("ru = '(Для характеристик)'");
	//		ОбъектНабора.Родитель        = Справочники.НаборыДополнительныхРеквизитовИСведений.Справочник_ХарактеристикиНоменклатуры;
	//		ОбъектНабора.ПометкаУдаления = Истина;
	//		ОбъектНабора.Записать();
	//		
	//		НаборСвойствХарактеристик = Справочники.НаборыДополнительныхРеквизитовИСведений.ПустаяСсылка();
	//		
	//	КонецЕсли;
	//	
	//КонецЕсли;
	//
КонецПроцедуры 

Процедура ОбновитьРеквизитНаборСвойствСерий()
	
	Если Не ЗначениеЗаполнено(НаборСвойствСерий) Тогда
		ОбъектНабора = Справочники.НаборыДополнительныхРеквизитовИСведений.СоздатьЭлемент();
	Иначе
		
		Если Не НаборСвойствНужноИзменить(НаборСвойствСерий) Тогда
			Возврат;
		КонецЕсли;
		
		ОбъектНабора = НаборСвойствСерий.ПолучитьОбъект();
		
	КонецЕсли;
	
	ОбъектНабора.Наименование    = Наименование + НСтр("ru = '(Для серий)'");
	ОбъектНабора.Родитель        = Справочники.НаборыДополнительныхРеквизитовИСведений.Справочник_СерииНоменклатуры;
	ОбъектНабора.ПометкаУдаления = ПометкаУдаления;
	ОбъектНабора.Записать();
	
	НаборСвойствСерий = ОбъектНабора.Ссылка;
	
КонецПроцедуры 

Процедура ОбновитьФлагиИспользованияСерийВСкладах()
	
	Если Не ПолучитьФункциональнуюОпцию("ИспользоватьСерииНоменклатуры") Тогда
		Возврат;
	КонецЕсли;
	
	УстановитьПривилегированныйРежим(Истина);
	
	Запрос = Новый Запрос;
	Запрос.Текст =
	"ВЫБРАТЬ
	|	ВидыНоменклатурыПолитикиУчетаСерий.Склад КАК Склад,
	|	ИСТИНА КАК ИспользоватьСерииНоменклатуры,
	|	ВидыНоменклатурыПолитикиУчетаСерий.ПолитикаУчетаСерий.УказыватьПриПланированииОтбора КАК КонтролироватьОперативныеОстатки
	|ИЗ
	|	Справочник.СТЦ_ВидыНоменклатуры.ПолитикиУчетаСерий КАК ВидыНоменклатурыПолитикиУчетаСерий
	|ГДЕ
	|	((НЕ ВидыНоменклатурыПолитикиУчетаСерий.Склад.ИспользоватьСерииНоменклатуры)
	|			ИЛИ (НЕ ВидыНоменклатурыПолитикиУчетаСерий.Склад.КонтролироватьОперативныеОстатки)
	|				И ВидыНоменклатурыПолитикиУчетаСерий.ПолитикаУчетаСерий.УказыватьПриПланированииОтбора)
	|	И ВидыНоменклатурыПолитикиУчетаСерий.Ссылка = &Ссылка
	|
	|ОБЪЕДИНИТЬ ВСЕ
	|
	|ВЫБРАТЬ РАЗЛИЧНЫЕ
	|	Склады.Ссылка,
	|	ЛОЖЬ,
	|	ЛОЖЬ
	|ИЗ
	|	Справочник.Склады КАК Склады
	|		ЛЕВОЕ СОЕДИНЕНИЕ Справочник.СТЦ_ВидыНоменклатуры.ПолитикиУчетаСерий КАК ВидыНоменклатурыПолитикиУчетаСерий
	|		ПО (ВидыНоменклатурыПолитикиУчетаСерий.Склад = Склады.Ссылка)
	|ГДЕ
	|	Склады.ИспользоватьСерииНоменклатуры
	|	И ВидыНоменклатурыПолитикиУчетаСерий.Ссылка ЕСТЬ NULL ";
	Запрос.УстановитьПараметр("Ссылка", Ссылка);
	
	Выборка = Запрос.Выполнить().Выбрать();
	
	Пока Выборка.Следующий() Цикл
		СкладОбъект = Выборка.Склад.ПолучитьОбъект();
		СкладОбъект.ИспользоватьСерииНоменклатуры = Выборка.ИспользоватьСерииНоменклатуры;
		
		Если Выборка.КонтролироватьОперативныеОстатки Тогда
			СкладОбъект.КонтролироватьОперативныеОстатки = Истина;
			СкладОбъект.УдалитьОтключитьКонтрольОперативныхОстатков = Не СкладОбъект.КонтролироватьОперативныеОстатки; //Для обратной совместимости
		КонецЕсли;
		
		СкладОбъект.ДополнительныеСвойства.Вставить("ПропуститьОбновлениеФлагаИспользованияСерий");
		СкладОбъект.ДополнительныеСвойства.Вставить("ПропуститьОбновлениеФлагаКонтроляОперативныхОстатков");
		
		СкладОбъект.Записать();
	КонецЦикла;
	
КонецПроцедуры

