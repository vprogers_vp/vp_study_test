﻿

//СТЦ a.neumyvakin 20201024
Функция ПолучитьПризнакиУдаленияСообщения(id) Экспорт
	
	Результат = Новый Структура;
	Результат.Вставить("ПометкаУдаления", Ложь);
	Результат.Вставить("Удалено", Ложь);
	
	Запрос = Новый Запрос;
	Запрос.Текст = 
	"ВЫБРАТЬ ПЕРВЫЕ 1
	|	СТЦ_УдалениеСообщенийPostlink.id КАК id,
	|	СТЦ_УдалениеСообщенийPostlink.Удалено КАК Удалено
	|ИЗ
	|	РегистрСведений.СТЦ_УдалениеСообщенийPostlink КАК СТЦ_УдалениеСообщенийPostlink
	|ГДЕ
	|	СТЦ_УдалениеСообщенийPostlink.id = &id
	|";
	
	Запрос.УстановитьПараметр("id", id);
	
	Рез = Запрос.Выполнить().Выбрать();
	
	Если Рез.Следующий() Тогда
		
		Результат.ПометкаУдаления = Истина;
		Результат.Удалено = Рез.Удалено;
		
		Возврат Результат;
	КонецЕсли;
	
	Возврат Результат;
КонецФункции


//СТЦ a.neumyvakin 20201024
Процедура УстановитьПометкуУдаления(id) Экспорт
	
	МЗ = РегистрыСведений.СТЦ_УдалениеСообщенийPostlink.СоздатьМенеджерЗаписи();
	МЗ.id = id;
	МЗ.Записать();
КонецПроцедуры


//СТЦ a.neumyvakin 20201024
Процедура СнятьПометкуУдаления(id) Экспорт
	
	НЗ = РегистрыСведений.СТЦ_УдалениеСообщенийPostlink.СоздатьНаборЗаписей();
	НЗ.Отбор.id.Установить(id);
	НЗ.Записать();
КонецПроцедуры


//СТЦ a.neumyvakin 20201119
Процедура ИзменитьПризнакУдаления(id, ПризнакУдаления) Экспорт
	
	МЗ = РегистрыСведений.СТЦ_УдалениеСообщенийPostlink.СоздатьМенеджерЗаписи();
	МЗ.id = id;
	МЗ.Прочитать();
	
	Если МЗ.Выбран() Тогда
		
		МЗ.Удалено = ПризнакУдаления;
		МЗ.Записать(Истина);
	КонецЕсли;
КонецПроцедуры



