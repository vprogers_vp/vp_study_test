﻿
&НаСервереБезКонтекста
Процедура ЗагрузитьПопвещенияОПревышенииЛимитаНаСервере(макет)
	Пегас = ПолучитьПодключениеКСервернойБД();
	//Пегас = ПолучитьПодключениеКФайловойБД();
	запросПоЛимитам = Пегас.NewObject("Запрос");
	запросПоЛимитам.Текст = "ВЫБРАТЬ
	|	ЗатратыПоПроектамОбороты.Проект,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаНаСогласованииОборот) КАК СуммаЗатратНаСогласовании,
	|	СУММА(ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА 0
	|			ИНАЧЕ ЗатратыПоПроектамОбороты.СуммаОборот
	|		КОНЕЦ) КАК СуммаЗатратСогласовано,
	|	СУММА(ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА ЗатратыПоПроектамОбороты.СуммаОборот
	|			ИНАЧЕ 0
	|		КОНЕЦ) КАК КорректировкаОплаты,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаНаСогласованииОборот + ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА 0
	|			ИНАЧЕ ЗатратыПоПроектамОбороты.СуммаОборот
	|		КОНЕЦ) КАК СуммаЗатрат,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаЗаказыПоставщикамОборот) КАК СуммаЗП,
	|	СУММА(-ЗатратыПоПроектамОбороты.СуммаВозвратовОплатыОборот) КАК СуммаВозвратовОплаты,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаКорректировокПКИНаСкладахОборот) КАК СуммаКорректировокПКИНаСкладах,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаОборот - ЗатратыПоПроектамОбороты.СуммаВозвратовОплатыОборот + ЗатратыПоПроектамОбороты.СуммаКорректировокПКИНаСкладахОборот) КАК СуммаСогласованоУчетомВозвратовИКорректировок
	|ИЗ
	|	РегистрНакопления.ЗатратыПоПроектам.Обороты(, , Регистратор, ) КАК ЗатратыПоПроектамОбороты
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗатратыПоПроектамОбороты.Проект
	|
	|УПОРЯДОЧИТЬ ПО
	|	ЗатратыПоПроектамОбороты.Проект.ДатаНачалаПлан УБЫВ";
	рез = запросПоЛимитам.Выполнить().Выгрузить();
	для каждого строка из рез Цикл
		лимитПоПроекту = строка.Проект.СуммаДляРасчетаЛимитовЗатрат*строка.Проект.ПроцентЛимитаЗатратОбщий/100; //Так в отчете
		ИтогоСогласовано = строка.СуммаСогласованоУчетомВозвратовИКорректировок;
		Если ИтогоСогласовано > лимитПоПроекту 
			и (строка.Проект.ВидСделки.Description = "Поставка" или строка.Проект.ВидСделки.Description = "НИР"  или строка.Проект.ВидСделки.Description = "Инициативный") 
			и ((ЗначениеЗаполнено(Строка.Проект.ДатаОкончания) и Строка.Проект.ДатаОкончания > ТекущаяДата()) или не ЗначениеЗаполнено(Строка.Проект.ДатаОкончания)) 
			и Строка.Проект.датаНачала > Дата('20180101') 
			и лимитПоПроекту > 0 Тогда
			сообщить(">>>>>>>>>>>>>>>>>>>>>>"+символы.ПС+"Обработка проекта "+Строка.Проект.наименование);
			
			Проект = ПолучитьПроект(Строка.Проект);
			
			имяФайла = ПолучитьФайл(Пегас,Строка.Проект,макет);
			
			Оповещение = СоздатьОповещение(Проект,лимитПоПроекту,ИтогоСогласовано,имяФайла);
			
			Если ЗначениеЗаполнено(Оповещение) Тогда 
				сообщить("Создано оповещение по проекту "+Строка.Проект.наименование);
				БП = создатьБизнесПроцесс(оповещение); 
				//прервать;
			КонецЕсли;			
			
		КонецЕсли;		
	КонецЦикла;	
КонецПроцедуры

&НаСервере
Функция ПолучитьМакетНаСервере()
	обработкаОбъект = реквизитФормыВЗначение("Объект");
	возврат обработкаОбъект.ПолучитьМакет("Макет");
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьФайл(Пегас,Проект,макет)
	запросПоЛимитам = Пегас.NewObject("Запрос");
	запросПоЛимитам.Текст = "ВЫБРАТЬ
	|	ЗатратыПоПроектамОбороты.Проект,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаНаСогласованииОборот) КАК СуммаЗатратНаСогласовании,
	|	СУММА(ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА 0
	|			ИНАЧЕ ЗатратыПоПроектамОбороты.СуммаОборот
	|		КОНЕЦ) КАК СуммаЗатратСогласовано,
	|	СУММА(ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА ЗатратыПоПроектамОбороты.СуммаОборот
	|			ИНАЧЕ 0
	|		КОНЕЦ) КАК КорректировкаОплаты,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаНаСогласованииОборот + ВЫБОР
	|			КОГДА ЗатратыПоПроектамОбороты.Регистратор ССЫЛКА Документ.КорректировкаРегистров
	|				ТОГДА 0
	|			ИНАЧЕ ЗатратыПоПроектамОбороты.СуммаОборот
	|		КОНЕЦ) КАК СуммаЗатрат,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаЗаказыПоставщикамОборот) КАК СуммаЗП,
	|	СУММА(-ЗатратыПоПроектамОбороты.СуммаВозвратовОплатыОборот) КАК СуммаВозвратовОплаты,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаКорректировокПКИНаСкладахОборот) КАК СуммаКорректировокПКИНаСкладах,
	|	СУММА(ЗатратыПоПроектамОбороты.СуммаОборот - ЗатратыПоПроектамОбороты.СуммаВозвратовОплатыОборот + ЗатратыПоПроектамОбороты.СуммаКорректировокПКИНаСкладахОборот) КАК СуммаСогласованоУчетомВозвратовИКорректировок,
	|	ЗатратыПоПроектамОбороты.ЗаказПоставщику
	|ИЗ
	|	РегистрНакопления.ЗатратыПоПроектам.Обороты(, , Регистратор, ) КАК ЗатратыПоПроектамОбороты
	|ГДЕ
	|	ЗатратыПоПроектамОбороты.Проект = &Проект
	|
	|СГРУППИРОВАТЬ ПО
	|	ЗатратыПоПроектамОбороты.Проект,
	|	ЗатратыПоПроектамОбороты.ЗаказПоставщику
	|
	|УПОРЯДОЧИТЬ ПО
	|	ЗатратыПоПроектамОбороты.Проект.ДатаНачалаПлан УБЫВ";
	ЗапросПоЛимитам.УстановитьПараметр("Проект",Проект);
	рез = запросПоЛимитам.Выполнить().Выгрузить();
	ТабДок = новый ТабличныйДокумент;
	ОбластьШапка = Макет.получитьОбласть("Шапка");
	ОбластьШапка.Параметры.Проект = проект.наименование;
	ОбластьШапка.Параметры.Дата = Формат(ТекущаяДата(),"ДФ='dd ММММ yyyy'")+"г.";
	ОбластьШапка.Параметры.лимит = Проект.СуммаДляРасчетаЛимитовЗатрат*Проект.ПроцентЛимитаЗатратОбщий/100;
	ОбластьШапка.Параметры.СуммаСогласованныхЗаказов = рез.Итог("СуммаЗатратСогласовано");
	ОбластьШапка.Параметры.СуммаСогласованныхЗаказовСУчетомКорректировок = рез.Итог("СуммаСогласованоУчетомВозвратовИКорректировок");
	ТабДок.Вывести(ОбластьШапка);
	СуммаПодвала = 0;
	ТабДок.НачатьАвтогруппировкуСтрок();
	для каждого строка из рез Цикл
		ОбластьЗаказ = Макет.получитьОбласть("Заказ");
		областьЗаказ.параметры.ЗаказПоставщику  = строка.заказпоставщику.партнер.наименованиеПолное+" / "+ "Заказ №" + строка.заказпоставщику.номер +" от "+формат(строка.заказпоставщику.дата, "ДФ=dd.MM.yyyy");
		областьЗаказ.параметры.Сумма =  строка.СуммаЗатратСогласовано;
		СуммаПодвала = СуммаПодвала + строка.СуммаЗатратСогласовано;
		ТабДок.Вывести(ОбластьЗаказ,1);
		для каждого стр из Строка.ЗаказПоставщику.РаспределениеПоПроектам Цикл
			Если стр.Проект.код = проект.код Тогда
				ОбластьНоменклатура = Макет.получитьОбласть("Номенклатура");
				ОбластьНоменклатура.параметры.номенклатура = стр.Номенклатура.наименованиеПолное;
				ОбластьНоменклатура.параметры.Количество = стр.количество;
				строкаЗапасов = Строка.заказПоставщику.товары.найти(стр.КодСтроки,"КодСтроки");
				Если не СтрокаЗапасов = неопределено тогда
					ОбластьНоменклатура.параметры.Сумма = стр.количество*СтрокаЗапасов.СуммаСНДС/СтрокаЗапасов.количество;
				КонецЕсли;
				ТабДок.Вывести(ОбластьНоменклатура,2);
			КонецЕсли;			
		КонецЦикла;		
	КонецЦикла;	
	ТабДок.ЗакончитьАвтогруппировкуСтрок();
	ОбластьПодвал = Макет.получитьОбласть("Подвал");
	ОбластьПодвал.параметры.Сумма =  СуммаПодвала;
	ТабДок.Вывести(ОбластьПодвал);	
	
	НаименованиеПроекта = Проект.наименование;
	УбратьЗапрещенныеСимволы(НаименованиеПроекта);
	ИмяФайла = каталогВременныхФайлов()+НаименованиеПроекта+".xls";
	ТабДок.Записать(ИмяФайла,ТипФайлаТабличногоДокумента.XLS);
	ДвоичныеДанные = новый ДвоичныеДанные(имяФайла);
	адрес = поместитьВоВременноеХранилище(ДвоичныеДанные);
	возврат ИмяФайла;
КонецФункции

&НаСервереБезКонтекста
Функция создатьБизнесПроцесс(СсылкаНаОповещение)
	бп = БизнесПроцессы.Ознакомление.СоздатьБизнесПроцесс();
	ШаблонОзнакомления = справочники.ШаблоныОзнакомления.НайтиПоКоду("ГЛ-00000000000000000000000000000000000000000000019");
	ПользовательБот = пользователи.ТекущийПользователь();
	БП.Проект = СсылкаНаОповещение.проект;
	бп.ЗаполнитьПоШаблонуИПредмету(ШаблонОзнакомления, СсылкаНаОповещение, ПользовательБот);	
	бп.Дата = ТекущаяДата();
	бп.Наименование = СтрШаблон("Оповещение о превышении лимита по проекту %1", СсылкаНаОповещение.Проект.наименование);
	бп.Записать();
	бп.Старт();
	сообщить("Запущен процесс оповешения о превышении лимита");
	возврат БП.Ссылка;
КонецФункции

&НаСервереБезКонтекста
Функция СоздатьОповещение(Проект,ЛимитПоПроекту,ИтогоСогласовано,ИмяФайла)
	оповещение = справочники.ВнутренниеДокументы.СоздатьЭлемент();
	запрос = новый запрос("ВЫБРАТЬ
	                      |	ВнутренниеДокументы.Ссылка КАК Ссылка
	                      |ИЗ
	                      |	Справочник.ВнутренниеДокументы КАК ВнутренниеДокументы
	                      |ГДЕ
	                      |	ВнутренниеДокументы.Проект = &Проект
	                      |	И ВнутренниеДокументы.ДатаСоздания > &ДатаСоздания
	                      |	И НЕ ВнутренниеДокументы.ПометкаУдаления");
	Запрос.УстановитьПараметр("Проект",Проект);
	Запрос.УстановитьПараметр("ДатаСоздания",ТекущаяДата()-24*60*60*7);
	рез = запрос.Выполнить().Выбрать();
	если рез.Следующий() тогда
		Сообщить("Оповещение по проекту "+Проект+" не создано, так как по этом проекту уже существует документ. "+символы.ПС+рез.ссылка);
		возврат неопределено;
	КонецЕсли;
	запрос = новый запрос("ВЫБРАТЬ
	                      |	ТекущиеСостоянияДокументов.Документ КАК Документ,
	                      |	ТекущиеСостоянияДокументов.Состояние КАК Состояние
	                      |ИЗ
	                      |	РегистрСведений.ТекущиеСостоянияДокументов КАК ТекущиеСостоянияДокументов
	                      |ГДЕ
	                      |	ТекущиеСостоянияДокументов.Документ.Проект = &Проект
	                      |	И ТекущиеСостоянияДокументов.Состояние = &Состояние");
	запрос.УстановитьПараметр("Состояние",перечисления.СостоянияДокументов.НаСогласовании);
	Запрос.УстановитьПараметр("Проект",Проект);
	Запрос.УстановитьПараметр("ДатаСоздания",ТекущаяДата()-24*60*60);
	рез = запрос.Выполнить().Выбрать();
	если рез.Следующий() тогда
		Сообщить("Оповещение по проекту "+Проект+" не создано, так как по этом проекту уже существует документ. "+символы.ПС+рез.документ+ " в состоянии на согласовании");
		возврат неопределено;
	КонецЕсли;
	
	даныеЗаполнения = новый структура;
	даныеЗаполнения.Вставить("ШаблонДокумента",справочники.ШаблоныВнутреннихДокументов.НайтиПоКоду("ГЛ-000058"));
	даныеЗаполнения.Вставить("Основание",Неопределено);
	оповещение.Заполнить(даныеЗаполнения);
	оповещение.Проект = проект;
	Оповещение.Заголовок = оповещение.ВидДокумента.Наименование + " по проекту "+проект.наименование+" от "+Формат(ТекущаяДата(),"ДФ=dd.MM.yyyy")+"г.";
	Оповещение.Содержание = "По проекту "+ проект.наименование+" превышен лимит"+символы.ПС+
	"Текущий лимит составляет "+ ЛимитПоПроекту+"р."+символы.ПС+
	"Сумма согласованных затрат составляет "+ ИтогоСогласовано+"р."+символы.ПС+
	"Просьба проверить текущее состояние проекта и согласовать увеличение лимита";
	Оповещение.ДатаСоздания = текущаяДата();
	строка = оповещение.ДополнительныеРеквизиты.Добавить();
	строка.Свойство = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя","СТЦ_СуммаПроектаВСЕГО");
	строка.Значение = ЛимитПоПроекту;
	строка = оповещение.ДополнительныеРеквизиты.Добавить();
	строка.Свойство = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя","СТЦ_ЛимитИзрасходованСумма");
	строка.Значение = ИтогоСогласовано;
	строка = оповещение.ДополнительныеРеквизиты.Добавить();
	строка.Свойство = ПланыВидовХарактеристик.ДополнительныеРеквизитыИСведения.НайтиПоРеквизиту("Имя","СТЦ_ЛимитИзрасходованПроцентноеВыражение");
	Если ЛимитПоПроекту > 0 Тогда
		строка.Значение = окр(ИтогоСогласовано/ЛимитПоПроекту*100,2);
	КОнецЕсли;
	Оповещение.Записать();
	
	//СведенияОФайле = РаботаСФайламиКлиентСервер.СведенияОФайле("ФайлСВерсией");
	//СведенияОФайле.Комментарий = Проект.наименование;
	//СведенияОФайле.ИмяБезРасширения = Проект.наименование;
	//СведенияОФайле.РасширениеБезТочки = "xlsx";
	//СведенияОФайле.ХранитьВерсии = Истина;
	//СведенияОФайле.АдресВременногоХранилищаФайла = адрес;
	//СведенияОФайле.НоваяВерсияНомерВерсии = 1;
	//НовыйФайл = РаботаСФайламиВызовСервера.СоздатьФайл(Оповещение.ссылка, СведенияОФайле);
	//Версия = РаботаСФайламиВызовСервера.СоздатьВерсию(НовыйФайл, СведенияОФайле);
	//РаботаСФайламиВызовСервера.ОбновитьВерсиюВФайле(НовыйФайл, Версия, адрес);
	Файл = новый Файл(ИмяФайла);
	СведенияОФайле = РаботаСФайламиКлиентСервер.СведенияОФайле("ФайлСВерсией", Файл);
	ДвоичныеДанные = новый ДвоичныеДанные(ИмяФайла);
	адрес = поместитьВоВременноеХранилище(ДвоичныеДанные);
	СведенияОФайле.АдресВременногоХранилищаФайла = адрес;
	СведенияОФайле.АдресВременногоХранилищаТекста = адрес;
	//СведенияОФайле.ПараметрыРаспознавания = ПараметрыРаспознавания;
	СведенияОФайле.ХранитьВерсии = Истина;
	СведенияОФайле.ЗаписатьВИсторию = Истина;
	//СведенияОФайле.СписокКатегорий = СписокКатегорий;
	СведенияОФайле.НеобходимоВыполнитьВставкуШКНаКлиенте = ложь;
	//СведенияОФайле.ДополнительныеПараметры = ДополнительныеПараметры;
	СведенияОФайле.Проект = Проект;

	Док = РаботаСФайламиВызовСервера.СоздатьФайлСВерсией(Оповещение.ссылка, СведенияОФайле);

	возврат оповещение.Ссылка;
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьПроект(ПроектПегас)
	проект = справочники.Проекты.СоздатьЭлемент();
	запрос = новый запрос("ВЫБРАТЬ
	                      |	Проекты.Ссылка КАК Ссылка
	                      |ИЗ
	                      |	Справочник.Проекты КАК Проекты
	                      |ГДЕ
	                      |	Проекты.Код = &Код");
	запрос.УстановитьПараметр("Код",ПроектПегас.Код);
	рез = запрос.Выполнить().Выбрать();
	если рез.Следующий() Тогда
		Проект = рез.ссылка.ПолучитьОбъект();
	ИНаче
		запрос = новый запрос("ВЫБРАТЬ
		                      |	Проекты.Ссылка КАК Ссылка
		                      |ИЗ
		                      |	Справочник.Проекты КАК Проекты
		                      |ГДЕ
		                      |	Проекты.Наименование = &Наименование");
		запрос.УстановитьПараметр("Наименование",ПроектПегас.Наименование);
		рез = запрос.Выполнить().Выбрать();
		Если рез.Следующий() тогда
			Проект = рез.ссылка.ПолучитьОбъект();
		КонецЕсли;		
	КонецЕсли;
	Проект.наименование = ПроектПегас.Наименование;
	Проект.Код = ПроектПегас.Код;
	проект.Руководитель = ПолучитьПользователя(?(ЗначениеЗаполнено(ПроектПегас.ответственныйЗаРасходы.наименование),ПроектПегас.ответственныйЗаРасходы,ПроектПегас.ответственный));
	проект.СТЦ_КураторПроекта = ПолучитьПользователя(ПроектПегас.Куратор);
	проект.Заказчик = ПолучитьКонтрагента(ПроектПегас);
	Проект.Организация = ПолучитьОрганизацию(ПроектПегас);
	Проект.ВидПроекта = ПолучитьВидПроекта(ПроектПегас.ВидСделки);
	Проект.Папка = ПолучитьПапкуПроекта(ПроектПегас.ВидСделки,ПроектПегас.ДатаНачала);
	проект.СпособПланирования = перечисления.СпособыПланированияПроекта.ОтДатыНачалаПроекта;
	Проект.ЕдиницаТрудозатратЗадач = перечисления.ЕдиницыТрудозатрат.ЧеловекоЧас;
	Проект.ЕдиницаДлительностиЗадач = перечисления.ЕдиницыДлительности.Час;
	Проект.ТекущийПланНачало = ?(ЗначениеЗаполнено(ПроектПегас.датаНачала),ПроектПегас.датаНачала,ТекущаяДата());
	Проект.ТекущийПланОкончание = ?(ЗначениеЗаполнено(ПроектПегас.датаОкончания),ПроектПегас.ДатаОкончания,ТекущаяДата());
	Проект.СписыватьЗатратыНаПроект = истина;
	проект.состояние = перечисления.СостоянияПроектов.Исполняется;
	Проект.записать();
	возврат Проект.ссылка;
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьПапкуПроекта(ВидПроектаПегас,ДатаНачала)
	наименование = ВидПроектаПегас.наименование+" "+формат(ДатаНАчала,"ДФ=yyyy");
	ПапкаПроекта = справочники.ПапкиПроектов.СоздатьЭлемент();
	Запрос = новый запрос("ВЫБРАТЬ
	                      |	ПапкиПроектов.Ссылка КАК Ссылка
	                      |ИЗ
	                      |	Справочник.ПапкиПроектов КАК ПапкиПроектов
	                      |ГДЕ
	                      |	ПапкиПроектов.Наименование = &Наименование");
	запрос.УстановитьПараметр("Наименование",Наименование);
	рез = запрос.Выполнить().Выбрать();
	Если рез.Следующий() Тогда
		ПапкаПроекта = рез.ссылка.ПолучитьОбъект();
	КонецЕсли;
	ПапкаПроекта.Наименование = наименование;
	//ВидПроекта. =  ПользовательПегас.ИдентификаторPostLink;
	ПапкаПроекта.Записать();
	возврат ПапкаПроекта.Ссылка;
КонецФункции

&НаСервереБезКонтекста
Процедура УбратьЗапрещенныеСимволы(Строка)

	МассивСимволов = Новый Массив;            
	МассивСимволов.Добавить(Символ(34)); //"  
	МассивСимволов.Добавить("\");
	МассивСимволов.Добавить("/");
	МассивСимволов.Добавить(":");
	МассивСимволов.Добавить("*");
	МассивСимволов.Добавить("?");
	МассивСимволов.Добавить("<");
	МассивСимволов.Добавить(">");
	МассивСимволов.Добавить("|");
	
	Для каждого Симв из МассивСимволов Цикл
		Строка = СтрЗаменить(Строка, Симв, "");
	КонецЦикла;
	
КонецПроцедуры

&НаСервереБезКонтекста
Функция ПолучитьВидПроекта(ВидПроектаПегас)
	ВидПроекта = справочники.ВидыПроектов.СоздатьЭлемент();
	Запрос = новый запрос("ВЫБРАТЬ
	                      |	ВидыПроектов.Ссылка КАК Ссылка
	                      |ИЗ
	                      |	Справочник.ВидыПроектов КАК ВидыПроектов
	                      |ГДЕ
	                      |	ВидыПроектов.Наименование = &Наименование");
	запрос.УстановитьПараметр("Наименование",ВидПроектаПегас.Наименование);
	рез = запрос.Выполнить().Выбрать();
	Если рез.Следующий() Тогда
		ВидПроекта = рез.ссылка.ПолучитьОбъект();
	КонецЕсли;
	ВидПроекта.Наименование = ВидПроектаПегас.наименование;
	//ВидПроекта. =  ПользовательПегас.ИдентификаторPostLink;
	ВидПроекта.Записать();
	возврат ВидПроекта.Ссылка;
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьОрганизацию(ОрганизацияПегас)
	
КонецФункции


&НаСервереБезКонтекста
Функция ПолучитьКонтрагента(КонтрагентПегас)
	
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьПользователя(ПользовательПегас)
	пользователь = Справочники.Пользователи.СоздатьЭлемент();
	Запрос = новый запрос("ВЫБРАТЬ
	                      |	Пользователи.Ссылка КАК Ссылка
	                      |ИЗ
	                      |	Справочник.Пользователи КАК Пользователи
	                      |ГДЕ
	                      |	Пользователи.Наименование = &Наименование");
	запрос.УстановитьПараметр("Наименование",ПользовательПегас.Наименование);
	рез = запрос.Выполнить().Выбрать();
	Если рез.Следующий() Тогда
		пользователь = рез.ссылка.ПолучитьОбъект();
	КонецЕсли;
	Пользователь.Наименование = ПользовательПегас.наименование;
	//Пользователь. =  ПользовательПегас.ИдентификаторPostLink;
	Пользователь.Записать();
	возврат пользователь.Ссылка;	
КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьПодключениеКФайловойБД()
	
	Параметры = "File=d:\1C_Bases\СТЦ\Пегас;Usr=МОРЯКИН;Pwd=;";
	V83COMConnector= Новый COMОбъект("V83.COMConnector");
	Попытка
		Возврат V83COMConnector.Connect(Параметры);
	Исключение
		Сообщить("Ошибка подключения!");
		Возврат Неопределено;
	КонецПопытки;

КонецФункции

&НаСервереБезКонтекста
Функция ПолучитьПодключениеКСервернойБД()
	
	Параметры = "Srvr=""srvdocmng01"";Ref=""Pegas_sf"";Usr=""backup"";Pwd=""OnWhomTheMoonDothShine.849"";";
	V83COMConnector= Новый COMОбъект("V83.COMConnector");
	Попытка
		Возврат V83COMConnector.Connect(Параметры);
	Исключение
		Сообщить(ОписаниеОшибки());
		Возврат Неопределено;
	КонецПопытки;
КонецФункции

&НаКлиенте
Процедура ЗагрузитьПопвещенияОПревышенииЛимита(Команда)
	ЗагрузитьПопвещенияОПревышенииЛимитаНаСервере(ПолучитьМакетНаСервере());
КонецПроцедуры
