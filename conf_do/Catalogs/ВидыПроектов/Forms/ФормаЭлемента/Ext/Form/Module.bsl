﻿#Область ОбработчикиСобытийЭлементовФормы

&НаКлиенте
Процедура КомментарийНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	
	ОбщегоНазначенияКлиент.ПоказатьФормуРедактированияКомментария(Элемент.ТекстРедактирования,
		ЭтотОбъект, "Объект.Комментарий")
	
КонецПроцедуры
	
#КонецОбласти

#Область ОбработчикиКомандФормы

&НаКлиенте
Процедура ОткрытьКТ(Команда)
	
	Если Объект.Ссылка.Пустая() Тогда 
		
		Если Не Записать() Тогда 
			Возврат;
		КонецЕсли;	
		
		ПоказатьОповещениеПользователя(
			НСтр("ru = 'Создание:'"),
			ПолучитьНавигационнуюСсылку(Объект.Ссылка),
			Строка(Объект.Ссылка),
			БиблиотекаКартинок.Информация32);
			
	КонецЕсли;	
		
	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("ОбъектКТ", Объект.Ссылка);
	ОткрытьФорму("Справочник.ШаблоныКонтрольныхТочек.Форма.ШаблонКТОбъекта", ПараметрыФормы, ЭтаФорма, Объект.Ссылка);
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьНаборСвойствПроектов(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения(
		"ОткрытьНаборСвойств",
		ЭтотОбъект,
		Новый Структура("НаборСвойств", Объект.НаборСвойствПроектов));

	Если Объект.Ссылка.Пустая() Тогда 
		
		ТекстВопроса = НСтр("ru = 'Для перехода к набору свойств элемент необходимо записать.'") 
			+ Символы.ПС + НСтр("ru = 'Записать?'");
			
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	Иначе 
		ВыполнитьОбработкуОповещения(ОписаниеОповещения, КодВозвратаДиалога.Ок);
	КонецЕсли;
	
КонецПроцедуры

&НаКлиенте
Процедура ОткрытьНаборСвойств(Результат, Параметры) Экспорт 
	
	Если Результат = КодВозвратаДиалога.Нет Тогда 
		Возврат;
	ИначеЕсли Результат = КодВозвратаДиалога.Да Тогда
		
		Если Не Записать() Тогда 
			Возврат;
		КонецЕсли;
	КонецЕсли;

	ПараметрыФормы = Новый Структура;
	ПараметрыФормы.Вставить("КлючНазначенияИспользования", "НаборыДополнительныхРеквизитов");
	
	ОткрытьФорму("Справочник.НаборыДополнительныхРеквизитовИСведений.ФормаСписка", ПараметрыФормы);
	
	ПараметрыПерехода = Новый Структура;
	ПараметрыПерехода.Вставить("Набор", Параметры.НаборСвойств);
	ПараметрыПерехода.Вставить("Свойство", Неопределено);
	ПараметрыПерехода.Вставить("ЭтоДополнительноеСведение", Ложь);
	
	Оповестить("Переход_НаборыДополнительныхРеквизитовИСведений", ПараметрыПерехода);

КонецПроцедуры

&НаКлиенте
Процедура ОткрытьНаборСвойствПроектныхЗадач(Команда)
	
	ОписаниеОповещения = Новый ОписаниеОповещения(
		"ОткрытьНаборСвойств",
		ЭтотОбъект,
		Новый Структура("НаборСвойств", Объект.НаборСвойствПроектныхЗадач));

	Если Объект.Ссылка.Пустая() Тогда 
		
		ТекстВопроса = НСтр("ru = 'Для перехода к набору свойств элемент необходимо записать.'") 
			+ Символы.ПС + НСтр("ru = 'Записать?'");
			
		ПоказатьВопрос(ОписаниеОповещения, ТекстВопроса, РежимДиалогаВопрос.ДаНет);
	Иначе 
		ВыполнитьОбработкуОповещения(ОписаниеОповещения, КодВозвратаДиалога.Ок);
	КонецЕсли;
	
КонецПроцедуры

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	
	//СТЦ Ковальчук Начало блока 18.03.2021
	НовыйЭлемент = Элементы.Добавить("СТЦ_ФиксированнаяНоменклатураВКонтракте",Тип("ПолеФормы"),Элементы.ГруппаНастройки);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеФлажка;
	НовыйЭлемент.ПутьКДанным = "Объект.СТЦ_ФиксированнаяНоменклатураВКонтракте";
	НовыйЭлемент.ПоложениеЗаголовка = ПоложениеЗаголовкаЭлементаФормы.Право;
	//СТЦ Ковальчук Конец блока
	
	//СТЦ Ковальчук Начало блока 24.03.2021
	НовыйЭлемент = Элементы.Добавить("СТЦ_Папка",Тип("ПолеФормы"),Элементы.ГруппаНастройки);
	НовыйЭлемент.Вид = ВидПоляФормы.ПолеВвода;
	НовыйЭлемент.ПутьКДанным = "Объект.СТЦ_Папка";
	//СТЦ Ковальчук Конец блока
	
КонецПроцедуры

#КонецОбласти

