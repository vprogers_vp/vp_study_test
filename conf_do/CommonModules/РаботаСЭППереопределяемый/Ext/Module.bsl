﻿////////////////////////////////////////////////////////////////////////////////
// Модуль содержит процедуры и функции для работы с электронными подписями.
// 
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Возвращает полный путь к изображению отметки ЭП по описанию ЭП.
//
// Параметры:
//   ОписаниеЭП - Структура со свойствами:
//     Номер - Строка - серийный номер
//     Владелец - Строка - владелец сертификата (CN).
//     ДатаНачала - Дата - дата начала действия сертификата.
//     ДатаОкончания - Дата - дата окончания действия сертификата.
//
// Возвращаемое значение:
//   Строка - полный путь к файлу с изображением отметки ЭП.
//
//      пример кода	
//
//	МакетСертификат = ПолучитьОбщийМакет("ШаблонОтметкиЭП");
//	
//	Строки = Новый Массив;
//	Строки.Добавить(Новый Структура("Слева, Сверху, Текст", 260, 152,
//		Строка(ОписаниеЭП.Номер)));
//	Строки.Добавить(Новый Структура("Слева, Сверху, Текст", 260, 208,
//		Строка(ОписаниеЭП.Владелец)));
//	Строки.Добавить(Новый Структура("Слева, Сверху, Текст", 260, 262,
//		Формат(ОписаниеЭП.ДатаНачала, "ДФ=dd.MM.yyyy")));
//	Строки.Добавить(Новый Структура("Слева, Сверху, Текст", 550, 262,
//		Формат(ОписаниеЭП.ДатаОкончания, "ДФ=dd.MM.yyyy")));
//	ПутьКОтметкеЭП = РаботаСКартинками.НаложитьСтроки(МакетСертификат, "PNG", Строки, 36);
//	
//	Возврат ПутьКОтметкеЭП;
//
Функция СоздатьОтметкуЭП(ОписаниеЭП) Экспорт
	
	Возврат "";
	
КонецФункции

#КонецОбласти
