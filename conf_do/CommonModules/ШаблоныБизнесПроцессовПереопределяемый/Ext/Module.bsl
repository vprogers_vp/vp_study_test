﻿
////////////////////////////////////////////////////////////////////////////////
// Шаблоны бизнес процессов переопределяемый: модуль содержит переопределяемые процедуры и функции
//                                            для шаблонов процессов.
//  
////////////////////////////////////////////////////////////////////////////////

#Область ПрограммныйИнтерфейс

// Возвращает список пользовательских функций для автоподстановки исполнителей в шаблонах бизнес-процессов
// Параметры:
//	ИменаПредметовДляФункций - массив - массив имен предметов для функций автоподстановки
//
Функция ПолучитьСписокДоступныхФункций(ИменаПредметовДляФункций) Экспорт
	
	
	
	ДоступныеФункции = Новый СписокЗначений;
	//ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.<ИмяФункции>(Объект)", "<Представление функции>");
	//ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.ЗаместительРуководителяАвтораПроцесса(Объект)", "Заместитель руководителя автора процесса");
	
	
	
	
	//++СЛ
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_РуководительПроекта(Объект)", "Руководитель проекта (СТЦ)");
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_КураторПроекта(Объект)", "Куратор проекта (СТЦ)");
	//++СЛ
	
	
	//++ СТЦ - ПоповАА 18.06.2020 9:11:12
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_КураторДляПремии(Объект)", "Куратор подразделения (СТЦ)");
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_РуководительДоп_ПроектаКомандировка(Объект)", 
	"Руководитель доп - проекта (Командировка) (СТЦ)");
	//+ СТЦ - ПоповАА 18.06.2020 9:11:21
	
	
	//++ СТЦ, Начало изменений -  ПоповАА2 - 16.07.2020 
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_РуководительПодразделенияРуководителяДоп_Проекта(Объект)", 
	"Руководитель подразделения ""Руководителя доп - проекта"" (СТЦ)");
	//+ СТЦ, Конец изменений -  ПоповАА2 - 16.07.2020 
	
	
	//++ СТЦ, Начало изменений -  ПоповАА2 - 24.07.2020 
	//По основному реквезиту проект Внутренних документов
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_РуководительПроектаКомандировка(Объект)", 
	"Руководитель проекта  (Командировка) (СТЦ)");
	
	
	ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_РуководительПодразделенияРуководителяПроекта(Объект)", 
	"Руководитель подразделения ""Руководителя проекта"" (СТЦ)");
	//+ СТЦ, Конец изменений -  ПоповАА2 - 24.07.2020 
	
	
	Если Константы.СТЦ_РасширеннаяКарточкаКонтракта.Получить() = Истина Тогда    //+ СТЦ,  ПоповАА2 - 17.03.2021 
		
		//+ СТЦ,  ПоповАА2 - 17.03.2021 
		ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_ОтветственныйЗаЛимит_ПроектаЗатрат(Объект)", 
		"Ответственный за лимит (СТЦ)");
		
		
		//+ СТЦ,  ПоповАА2 - 17.03.2021 
		ДоступныеФункции.Добавить("ШаблоныБизнесПроцессовПереопределяемый.СТЦ_ОтветственныйЗаСогласованиеСчетов_ПроектаЗатрат(Объект)", 
		"Ответственный за согласование счетов (СТЦ)");
		
	КонецЕсли;
	
	
	
	Возврат ДоступныеФункции;
	
КонецФункции	

// Возвращает роль заместителя руководителя подразделения, в которое входит автор процесса.
// Функция является примером автоподстановки, возвращающей роль с объектом адресации.
//
// Параметры: 
//    БизнесПроцессОбъект - бизнес-процесс, в котором сработала автоподстановка
//
// Возвращаемое значение:
//    Структура со следующими полями:
//    РольИсполнителя – Справочники.РолиИсполнителей – роль "Заместитель руководителя подразделения"
//    ОсновнойОбъектАдресации - Характеристика.ОбъектыАдресацииЗадач - подразделение автора процесса
//    ДополнительныйОбъектАдресации - Характеристика.ОбъектыАдресацииЗадач - неопределено
//
//Функция ЗаместительРуководителяАвтораПроцесса(БизнесПроцессОбъект) Экспорт
//	
//	АвторБизнесПроцесса = БизнесПроцессОбъект.Автор;
//
//	Запрос = Новый Запрос;
//	Запрос.Текст = 
//	"ВЫБРАТЬ
//	|	СведенияОПользователяхДокументооборот.Подразделение КАК Подразделение
//	|ИЗ
//	|	РегистрСведений.СведенияОПользователяхДокументооборот КАК СведенияОПользователяхДокументооборот
//	|ГДЕ
//	|	СведенияОПользователяхДокументооборот.Пользователь = &Пользователь";
//	Запрос.УстановитьПараметр("Пользователь", АвторБизнесПроцесса);
//	
//	Результат = Запрос.Выполнить();
//	Подразделение = Неопределено;
//	
//	Если Не Результат.Пустой() Тогда 
//		Выборка = Результат.Выбрать();
//		Выборка.Следующий();
//		Подразделение = Выборка.Подразделение;
//	КонецЕсли;	
//	
//	СтруктураАдресации = Новый Структура;
//	СтруктураАдресации.Вставить("РольИсполнителя", 
//		Справочники.РолиИсполнителей.НайтиПоНаименованию("Заместитель руководителя подразделения"));
//	СтруктураАдресации.Вставить("ОсновнойОбъектАдресации", Подразделение);
//	СтруктураАдресации.Вставить("ДополнительныйОбъектАдресации", Неопределено);  
//		  	
//	Возврат СтруктураАдресации;
//	
//КонецФункции

// Вызывается из ШаблоныБизнесПроцессов.ОбновитьДоступностьЗависимыхШаблонов при
// обновлении доступности зависимых шаблонов.
//
// Параметры:
//  Шаблон - Структура - 
//  СтандартнаяОбработка – Булево - В случае значения Истина обновление доступности
//                         будет выполнено способом по умолчанию.
//
Процедура ПриОбновленииДоступностиЗависимыхШаблонов(Шаблон, СтандартнаяОбработка) Экспорт
	
	
	
КонецПроцедуры



Функция СТЦ_РуководительПроекта(БизнесПроцессОбъект) Экспорт
	
	РуководительПроекта = Справочники.Пользователи.ПустаяСсылка();
	ДопРеквизиты =  БизнесПроцессОбъект.Предметы[0].Предмет.ДополнительныеРеквизиты.Выгрузить();    //+ СТЦ,  ПоповАА2 - 21.10.2020 
	
	Проект = БизнесПроцессОбъект.Проект; 
	Если ЗначениеЗаполнено(Проект) Тогда 
		РуководительПроекта = Проект.Руководитель;
		//++ СТЦ, Начало изменений -  ПоповАА2 - 21.10.2020 	
	Иначе
		Для каждого Стр Из ДопРеквизиты Цикл
			
			Если Строка(Стр.Свойство.ТипЗначения) = "Проект" Тогда
				
				Если ЗначениеЗаполнено(Стр.Значение) Тогда
					
					РуководительПроекта = Стр.Значение.Руководитель;
					
				КонецЕсли;	
				
			КонецЕсли;	
		КонецЦикла;	
		//+ СТЦ, Конец изменений -  ПоповАА2 - 21.10.2020 
	КонецЕсли;
	
	Возврат РуководительПроекта
	
КонецФункции

Функция СТЦ_КураторПроекта(БизнесПроцессОбъект) Экспорт
	
	Куратор = Справочники.Пользователи.ПустаяСсылка();
	
	Проект = БизнесПроцессОбъект.Проект;
	Если ЗначениеЗаполнено(Проект) Тогда 
		Куратор = Проект.СТЦ_КураторПроекта;
	КонецЕсли;
	
	Возврат Куратор
	
КонецФункции


//++ СТЦ - ПоповАА 17.06.2020 17:23:03
Функция СТЦ_КураторДляПремии(БизнесПроцессОбъект) Экспорт
	
	Куратор = Справочники.Пользователи.ПустаяСсылка();
	ПодразделениеДопРеквизит = БизнесПроцессОбъект.Автор.Подразделение.ДополнительныеРеквизиты.Выгрузить();
	
	
	Если ПодразделениеДопРеквизит.Количество()>0 Тогда	
		Для каждого Стр Из ПодразделениеДопРеквизит Цикл
			Если Стр.Свойство.Имя = "Куратор_ebb3671d07ec4f8aa784e93010827cbf" Тогда
				Куратор = Стр.Значение;
			КонецЕсли;
		КонецЦикла; 
	КонецЕсли;
	
	Возврат Куратор;
	
КонецФункции

//По доп реквезиту Проект Внутренних документов
Функция СТЦ_РуководительДоп_ПроектаКомандировка(БизнесПроцессОбъект) Экспорт
	
	РуководительПроектаКомандировка = Справочники.Пользователи.ПустаяСсылка();
	ДопРеквезитыПроект = БизнесПроцессОбъект.Предметы[0].Предмет.ДополнительныеРеквизиты.Выгрузить();
	
	Для каждого Стр Из ДопРеквезитыПроект Цикл
		
		Если Строка(Стр.Свойство.ТипЗначения) = "Проект" Тогда
			РуководительПроектаКомандировка = Стр.Значение.Руководитель;					 
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат РуководительПроектаКомандировка;
	
КонецФункции
//+ СТЦ - ПоповАА 17.06.2020 17:23:08


//++ СТЦ, Начало изменений -  ПоповАА2 - 16.07.2020 
//По доп реквезиту Проект Внутренних документов
Функция СТЦ_РуководительПодразделенияРуководителяДоп_Проекта(БизнесПроцессОбъект) Экспорт
	
	РуководительПодразделенияРуководителяПроекта = Справочники.Пользователи.ПустаяСсылка();
	ДопРеквезитыПроект = БизнесПроцессОбъект.Предметы[0].Предмет.ДополнительныеРеквизиты.Выгрузить();
	ДопРеквизитыПодразделение = "";
	Для каждого Стр Из ДопРеквезитыПроект Цикл
		
		Если Строка(Стр.Свойство.ТипЗначения) = "Проект" Тогда
			РуководительПодразделенияРуководителяПроекта = Стр.Значение.Руководитель.Подразделение.Руководитель;					 
			Прервать;
		КонецЕсли;
		
	КонецЦикла;
	
	Возврат РуководительПодразделенияРуководителяПроекта;
	
КонецФункции
//+ СТЦ, Конец изменений -  ПоповАА2 - 16.07.2020 

//++ СТЦ, Начало изменений -  ПоповАА2 - 24.07.2020 
//По основному реквезиту Проект Внутренних документов

Функция СТЦ_РуководительПроектаКомандировка(БизнесПроцессОбъект) Экспорт
	
	РуководительПроектаКомандировка = Справочники.Пользователи.ПустаяСсылка();
	Проект = БизнесПроцессОбъект.Проект;
	РуководительПроектаКомандировка = Проект.Руководитель;
	
	Возврат РуководительПроектаКомандировка;
	
КонецФункции


Функция СТЦ_РуководительПодразделенияРуководителяПроекта(БизнесПроцессОбъект) Экспорт
	
	РуководительПодразделенияРуководителяПроекта = Справочники.Пользователи.ПустаяСсылка();
	Проект = БизнесПроцессОбъект.Проект;
	
	Если ЗначениеЗаполнено(Проект) Тогда
		РуководительПодразделенияРуководителяПроекта = Проект.Руководитель.Подразделение.Руководитель;
	КонецЕсли;
	
	Возврат РуководительПодразделенияРуководителяПроекта;
	
КонецФункции

//+ СТЦ, Конец изменений -  ПоповАА2 - 24.07.2020 


//+ СТЦ,  ПоповАА2 - 17.03.2021 
Функция СТЦ_ОтветственныйЗаЛимит_ПроектаЗатрат(БизнесПроцессОбъект) Экспорт
	
	ОтветственныйЗаЛимит = Справочники.Пользователи.ПустаяСсылка();
	
	Если  БизнесПроцессОбъект.Предметы.Количество() > 0 Тогда
		Документ = БизнесПроцессОбъект.Предметы[0].Предмет;
		ДокументОбъектМетаданные = Документ.ПолучитьОбъект().Метаданные();
		
		Если ОбщегоНазначения.ЕстьРеквизитОбъекта("СТЦ_ПроектЗатрат", ДокументОбъектМетаданные)  Тогда
			Если ЗначениеЗаполнено(Документ.СТЦ_ПроектЗатрат) Тогда
				ОтветственныйЗаЛимит = Документ.СТЦ_ПроектЗатрат.ОтветственныйЗаЛимит;
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;	
	
	Возврат ОтветственныйЗаЛимит;
	
КонецФункции


//+ СТЦ,  ПоповАА2 - 17.03.2021 
Функция СТЦ_ОтветственныйЗаСогласованиеСчетов_ПроектаЗатрат(БизнесПроцессОбъект) Экспорт
	
	ОтветственныйЗаСогласованиеСчетов = Справочники.Пользователи.ПустаяСсылка();
	
	Если  БизнесПроцессОбъект.Предметы.Количество() > 0 Тогда
		Документ = БизнесПроцессОбъект.Предметы[0].Предмет;
		ДокументОбъектМетаданные = Документ.ПолучитьОбъект().Метаданные();
		
		Если ОбщегоНазначения.ЕстьРеквизитОбъекта("СТЦ_ПроектЗатрат", ДокументОбъектМетаданные)  Тогда
			Если ЗначениеЗаполнено(Документ.СТЦ_ПроектЗатрат) Тогда
				ОтветственныйЗаСогласованиеСчетов = Документ.СТЦ_ПроектЗатрат.ОтветственныйЗаСогласованиеСчетов;
			КонецЕсли;
		КонецЕсли;
		
	КонецЕсли;	
	
	Возврат ОтветственныйЗаСогласованиеСчетов;
	
КонецФункции

#КонецОбласти
